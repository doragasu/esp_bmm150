#include <rom/ets_sys.h>
#include <core_i2c.h>

#include <esp_bmm150.h>

#define UNUSED __attribute__((unused))

static void delay_us(uint32_t t, UNUSED void *intf_ptr) {
	ets_delay_us(t);
}

static struct ci2c_intf bmm150_intf = {
	.dev_addr = 0x10
};

static struct bmm150_dev bmm150 = {
	.intf = BMM150_I2C_INTF,
	.delay_us = delay_us,
	.intf_ptr = &bmm150_intf,
	.read = (bmm150_read_fptr_t)ci2c_read,
	.write = (bmm150_write_fptr_t)ci2c_write
};

int bmm150_configure(void)
{
	const struct bmm150_settings settings = {
		.pwr_mode = BMM150_POWERMODE_NORMAL
	};

	int err = bmm150_init(&bmm150);
	if (!err) {
		err = bmm150_soft_reset(&bmm150);
	}
	if (!err) {
		err = bmm150_set_op_mode(&settings, &bmm150);
	}

	return err;
}

struct bmm150_dev *bmm150_dev_get(void)
{
	return &bmm150;
}
