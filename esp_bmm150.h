#ifndef __ESP_BMM150_H__
#define __ESP_BMM150_H__

#include <core_i2c.h>
#define BMM150_INTF_RET_TYPE esp_err_t
#include <bmm150.h>

int bmm150_configure(void);
struct bmm150_dev *bmm150_dev_get(void);

#endif /*__ESP_BMM150_H__*/
